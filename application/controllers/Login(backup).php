<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct() {
		parent::__construct();		
		$this->load->model('login_model');		
	}

	public function index()
	{
			if ($this->session->userdata('status') == 'login') {
				redirect('dashboard/home');
			}
			$this->load->view('login_view');
	}

	function salah(){
    	$this->session->set_flashdata('salah','Username atau Password Salah');
    	$this->load->view('login_view');
  	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'user_username' => $username,
			'user_password' => md5($password)
			);
		$cek = $this->login_model->cek_login('clinic_users',$where)->num_rows();
		if($cek > 0){
 
			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 
			$this->session->set_userdata($data_session);
 
			redirect(base_url("dashboard/home"));
 
		}else{
			redirect('login/salah');
		}
	}	

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
/* Location: ./application/controller/Login.php */