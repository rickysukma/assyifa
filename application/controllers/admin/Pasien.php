<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller{
	public function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        } else {
        	$this->load->library('template_dashboard');
        }
		// if(!$this->session->userdata('status')!= 'login') redirect(base_url());
		$this->load->library('template');
		$this->load->model('admin/dokter_model');
		$this->load->model('admin/pasien_model');
	}

	
	public function index()
	{

		$data['pasien'] = $this->pasien_model->select_all()->result();
		$this->template->display('admin/pasien_view',$data);
	}


	public function cari()
	{

		$data['pasien'] = $this->pasien_model->select_all()->result();
		$this->template->display('admin/pasien_cari',$data);
	}

	function caribydate(){
		$dari = $this->input->post('dari_tgl');
		$ke = $this->input->post('ke_tgl');
		if ($dari == NULL && $ke == NULL) {
			redirect('admin/pasien/carikunjungan');
		} else {
			redirect('admin/pasien/kunjungan/'.$dari.'/'.$ke);
		}
	}

	function kunjungan($min,$max)
	{
		if ($min == NULL && $max == NULL) {
			redirect('admin/pasien/carikunjungan');
		} else {

			$data['pasien'] = $this->pasien_model->select_by_date($min,$max);
			$this->template->display('admin/pasien_kunjungan',$data);
		}
	}

	function carikunjungan(){
		$data['pasien'] = $this->pasien_model->select_all_order_bydate()->result();
		$this->template->display('admin/carikunjungan',$data);	
	}

	function formulir(){
		$this->load->view('admin/formulir');
	}


}
/* Location: ./application/controller/admin/Home.php */