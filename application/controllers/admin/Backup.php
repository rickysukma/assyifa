<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller{
	public function __construct(){
		parent::__construct();
		// if(!$this->session->userdata('status')!= 'login') redirect(base_url());
		$this->load->library('template');
		$this->load->model('admin/home_model');
	}
	
	public function index()
	{
		if($this->session->userdata('role') == '9999')
			$this->template->display('admin/home_view', $data);
		}
		else
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
	}
}
/* Location: ./application/controller/admin/Home.php */