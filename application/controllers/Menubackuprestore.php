<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menubackuprestore extends CI_Controller{
// public function menubackuprestore(){
//         $this->load->model('nama_model');
//         $data['tabel'] = $this->nama_model->tampiltabel(); //AMBIL DATA TABEL-TABEL
//         $this->load->view('nama_view',$data);
//     }


public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('role') != '9999') {
          $this->session->set_flashdata('akses','Anda tidak memiliki hak Akses untuk halaman Backup/Restore');
          redirect(base_url('admin/home'));
        }
        $this->load->library('template_apotek');
    }
    
    public function index() {
        if($this->session->userdata('logged_in_clinic')){
            $this->template_apotek->display('apotek/backup_view');
        }
        else
        {
            $this->session->sess_destroy();
            redirect(base_url());
        }
    }

public function backup(){

      $this->load->dbutil();
      $prefs = array(     
                    'format'      => 'sql',             
                    'filename'    => 'my_db_backup.sql'
                  );
      $backup =& $this->dbutil->backup($prefs); 
      $db_name = 'backup-on-'. date("d-m-Y") .'.sql'; //NAMAFILENYA
      $save = 'db/'.$db_name;
      $this->load->helper('file');
      write_file($save, $backup); 
      $this->load->helper('download');
      force_download($db_name, $backup);
    }
public function restore(){

        $this->load->helper('file');
        $config['upload_path']="./db/";
        $config['allowed_types']="jpg|png|gif|jpeg|bmp|sql|x-sql";
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if(!$this->upload->do_upload("datafile")){
         $error = array('error' => $this->upload->display_errors());
         echo "GAGAL UPLOAD";
         var_dump($error);
         exit();
        }

        $file = $this->upload->data();  //DIUPLOAD DULU KE DIREKTORI assets/database/
        $fotoupload=$file['file_name'];
                    
          $isi_file = file_get_contents('./db/' . $fotoupload); //PANGGIL FILE YANG TERUPLOAD
          $string_query = rtrim( $isi_file, "\n;" );
          $array_query = explode(";", $string_query);   //JALANKAN QUERY MERESTORE KEDATABASE
              foreach($array_query as $query)
              {
                    $this->db->query($query);
              }

          $path_to_file = './db/' . $fotoupload;
            if(unlink($path_to_file)) {   // HAPUS FILE YANG TERUPLOAD
                 redirect('home/setting');
            }
            else {
                 echo 'errors occured';
            }
        
    }
}