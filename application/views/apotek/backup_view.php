<div class="page-content-wrapper">
    <div class="page-content">            
        <h3 class="page-title">
            Dashboard <small>Backup Restore</small>
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">                    
                <li>
                    <i class="fa fa-home"></i>
                    <a href="#">Backup Restore</a>
                </li>
            </ul>                
        </div>            
                        
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo site_url('menubackuprestore/backup'); ?>">
                    <button type="submit" style="width: 300px; height: 100px;" class="btn btn-primary"><i class="fa fa-save"></i><h3>Backup</h3></button>
                </a>
                
                <form>
                <?php echo form_open_multipart('menubackuprestore/restore');?>
                    <input type="file" name="datafile" id="datafile" />
                    <button type="submit" >Upload Database</button>
                </form>
            </div>
        </div>         
            
        <div class="clearfix"></div>
    </div>
</div>