<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/sweetalert2.css">
<script src="<?php echo base_url(); ?>js/sweetalert2.min.js"></script>
<?php 
if ($this->session->flashdata('akses')) { ?>
<script>
    swal({
        icon : "warning",
        title: "Hak Akses!",
        text: "<?php echo $this->session->flashdata('akses'); ?>",
        timer: 3000,
        showConfirmButton: false,
        type: 'warning'
    });
</script>
<? } ?>
<div class="page-content-wrapper">
    <div class="page-content">            
        <h3 class="page-title">
            Dashboard <small>Pasien</small>
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">                    
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?php echo site_url('dashboard/home'); ?>">Dashboard</a>
                </li>
            </ul>                
        </div>            
                        
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat blue-madison">
                    <div class="visual">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                        <!-- <?php 
                            $Jml_Pelanggan = count($TotalPelanggan);
                            echo number_format($Jml_Pelanggan);
                        ?> -->
                        </div>
                        <div class="desc">
                        Registrasi
                        </div>
                    </div>
                    <a class="more" href="<?php echo site_url('admin/pasien'); ?>">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat red-intense">
                    <div class="visual">
                        <i class="fa fa-search"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                        <!-- < ?php 
                            $Jml_Produk = count($TotalProduk);
                            echo number_format($Jml_Produk);
                        ?> -->
                        </div>
                        <div class="desc">
                        Cari Pasien
                        </div>
                    </div>
                    <a class="more" href="<?php echo site_url('admin/cari'); ?>">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                <div class="dashboard-stat green-haze">
                    <div class="visual">
                        <i class="fa fa-user-md"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                        </div>
                        <div class="desc">
                        Kunjungan Pasien
                        </div>
                    </div>
                    <a class="more" href="<?php echo site_url('admin/pasien/kunjungan'); ?>">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
        </div>         
            
        <div class="clearfix"></div>
    </div>
</div>