<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/sweetalert2.css">
<script src="<?php echo base_url(); ?>js/sweetalert2.min.js"></script>
<?php 
if ($this->session->flashdata('notif')) { ?>
<script>
    swal({
        title: "Gagal",
        text: "<?php echo $this->session->flashdata('notif'); ?>",
        timer: 3000,
        showConfirmButton: false,
        type: 'error'
    });
</script>
<? } ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#lstJenis").select2({
        });

        $("#lstKab").select2({
        });

        $("#lstKec").select2({
        });

        $("#lstGolongan").select2({
        });

        $("#lstPabrikan").select2({
        });

        $("#lstSuplier").select2({
        });
    });
</script>
<div class="page-content-wrapper">
    <div class="page-content">            
        <h3 class="page-title">
            Pasien <small>Tambah</small>
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">                    
                <li>
                    <i class="fa fa-user-plus"></i>
                    <a href="<?php echo site_url('admin/home'); ?>">Register</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Pasien</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?php echo site_url(''); ?>">Pasien</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Tambah Pasien</a>
                </li>
            </ul>                
        </div>            
                        
        <div class="row">
            <div class="col-md-12">

                <div class="portlet box red-intense">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-plus-square"></i> Form Tambah Pasien
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    
                    <div class="portlet-body form">
                        <form role="form" class="form-horizontal" action="<?php echo site_url('admin/register/savedata'); ?>" method="post" enctype="multipart/form-data" name="form1">
                        <input type="hidden">

                            <div class="form-body">
                            <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Tanggal Kunjungan</label>
                                    <div class="col-md-10">
                                        <input type="date" class="form-control" readonly="" id="form_control_1" value="<?php echo date('Y-m-d'); ?>" name="tgl_kunjungan" autocomplete="off" required autofocus>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">No. RM</label>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" value="<? echo sprintf("%06d", $maxid) ?>" id="form_control_1" name="no_rm" maxlength="2" autocomplete="off" required autofocus>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                                                
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Nama</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="form_control_1" placeholder="Masukan Nama Pasien" name="name" autocomplete="off" required autofocus>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Tempat Lahir</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="form_control_1" placeholder="Masukan Tempat Lahir" name="tmpt_lahir" autocomplete="off" required autofocus>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Tanggal Lahir</label>
                                    <div class="col-md-10">
                                        <input type="date" class="form-control" id="date" value="" name="tgl_lahir" autocomplete="off" required autofocus>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Umur</label>
                                    <div class="col-md-10">
                                        <input type="number" class="form-control" id="age" placeholder="Masukan Umur" name="umur" autocomplete="off" required autofocus>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">No Telepon</label>
                                    <div class="col-md-10">
                                        <input type="number" class="form-control" id="form_control_1" placeholder="Masukan Nomor Telepon" name="no_telp" autocomplete="off" required autofocus>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Alamat</label>
                                    <div class="col-md-10">
                                        <textarea rows="1" name="alamat" class="form-control"></textarea><br>
                                       <select class="select2_category form-control" data-placeholder="- Pilih Provinsi -" name="provinsi" id="lstJenis" required>
                                         <option value="">- Pilih Provinsi -</option>
                                            <?php foreach ($getDataProvinsi as $key): ?>
                                                <option value="<?php echo $key->nama ?>"><?php echo $key->nama ?></option>
                                            <?php endforeach ?>
                                       </select>
                                       <br><br><br>
                                       <select class="select2_category form-control" data-placeholder="- Pilih Kabupaten -" name="kabupaten" id="lstKab" required>
                                         <option value="">- Pilih Kabupaten -</option>
                                            <?php foreach ($getDataKabupaten as $key): ?>
                                                <option value="<?php echo $key->nama ?>"><?php echo $key->nama ?></option>
                                            <?php endforeach ?>
                                       </select>
                                       <br><br><br>
                                       <select class="select2_category form-control" data-placeholder="- Pilih Kecamatan -" name="kecamatan" id="lstKec" required>
                                         <option value="">- Pilih Kecamatan -</option>
                                            <?php foreach ($getDataKecamatan as $key): ?>
                                                <option value="<?php echo $key->nama ?>"><?php echo $key->nama ?></option>
                                            <?php endforeach ?>
                                       </select>

                                        
                                    </div>
                                        <div class="form-control-focus"></div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Jenis Kelamin</label>
                                    <div class="col-md-10">
                                        <label class="radio-inline">
                                          <input type="radio" name="jk" value="L">Laki - Laki
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="jk" value="P">Wanita
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Pekerjaan</label>
                                    <div class="col-md-10">
                                        <select name="pekerjaan" class="form-control select2_category">
                                            <option value="Belum Bekerja">Belum Bekerja</option>
                                            <option value="PNS">PNS</option>
                                            <option value="Wiraswasta">Wiraswasta</option>
                                            <option value="Buruh">Buruh</option>
                                            <option value="Polisi">Polisi</option>
                                            <option value="TNI">TNI</option>
                                            <option value="Guru">Guru</option>
                                        </select>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Pendidikan</label>
                                    <div class="col-md-10">
                                        <select name="pendidikan" class="form-control">
                                            <option value="">Pilih Jenjang Pendidikan terakhir . . . </option>
                                            <option value="Belum Sekolah">Belum Sekolah</option>
                                            <option value="SD">Sekolah Dasar</option>
                                            <option value="SMP">Sekolah Menengah Pertama</option>
                                            <option value="SMA">Sekolah Menengah Pertama</option>
                                            <option value="D1">Diploma 1</option>
                                            <option value="D2">Diploma 2</option>
                                            <option value="D3">Diploma 3</option>
                                            <option value="S1">Sarjana</option>
                                            <option value="S2">Magister</option>
                                            <option value="S3">Doktoral</option>
                                        </select>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Agama</label>
                                    <div class="col-md-10">
                                        <select class="form-control select2_category" name="agama">
                                            <option value="islam">Islam</option>
                                            <option value="kristen">Kristen</option>
                                            <option value="katolik">Katolik</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="budha">Budha</option>
                                        </select>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Staus</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="status">
                                            <option value="">Pilih Status...</option>
                                            <option value="nikah">Nikah</option>
                                            <option value="belum nikah">Belum Nikah</option>
                                            <option value="cerai hidup">Cerai Hidup</option>
                                            <option value="cerai mati">Cerai Mati</option>
                                        </select>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Klinik</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="klinik">
                                            <?php
                                                foreach ($klinik as $k) {
                                                     echo "<option value='$k->klinik'>$k->klinik</option>";
                                                 } 
                                            ?>
                                            <option value="LABORATORIUM">LABORATORIUM</option>
                                        </select>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Dokter</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="dokter">
                                            <option value="">Pilih Dokter....</option>
                                            <?php
                                                foreach ($dokter as $r) {
                                                    echo "<option value='$r->dokter_name'>$r->dokter_name</option>";
                                                }
                                            ?>
                                        </select>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label" for="form_control_1">Keluhan</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="keluhan" rows="5"></textarea>
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                </div> 
                                <!-- footer -->   
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-10">
                                        <!-- <a href="<?php echo site_url('admin/register/simpan'); ?>" class="btn btn-primary"><i class="fa fa-print"></i> Simpan</a> -->
                                        <button name="daftar" value="simpan" type="submit" class="btn green"><i class="fa fa-floppy-o"></i>Daftar</button>
                                        <button name="daftar" value="cetak" type="submit" class="btn yellow"><i class="fa fa-print"></i>Cetak</button>
                                        <a href="<?php echo site_url('admin/pasien'); ?>" class="btn red"><i class="fa fa-times"></i> Batal</a>
                                    </div>
                                </div>
                            </div>
                            <!-- batas -->
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>            
</div>  