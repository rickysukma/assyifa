<?php foreach ($pasien as $p) {
	# code...
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
  <script src="<?php echo base_url() ?>js/jquery.js"></script>
  <script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
</head>
<style type="text/css">
	table .center {
    margin-left:auto; 
    margin-right:auto;
  }
</style>
<body><center>

<div class="container">
  <h2>KLINIK AS-SYIFA</h2>
  <p>FORMULIR PENDAFTARAN REKAM MEDIS</p>
  <hr>                                                                                      
  <div class="table-responsive col-md-12">          
  <table class="center table table-striped" align="center">
      <tr>
      	<td>No RM</td><td>:</td><td><?php echo $p->no_rm ?></td>
      </tr>
      <tr>
        <td>Nama</td><td>:</td><td><?php echo $p->nama_pasien ?></td>
      </tr>
      <tr>
        <td>Tempat, Tanggal Lahir</td><td>:</td><td><?php $date = $p->tanggal_lahir; echo $p->tempat_lahir.", ".date("d-m-Y", strtotime($date)); ?></td>
      </tr>
      <tr>
        <td>Umur</td><td>:</td><td><?php echo $p->umur ?> Tahun</td>
      </tr>
      <tr>
        <td>Jenis Kelamin</td><td>:</td><td><?php switch ($p->jenis_kelamin) {
        	case 'L':
        		echo "Laki - Laki";
        		break;
        	
        	default:
        		echo "Perempuan";
        		break;
        }$p->jenis_kelamin ?></td>
      </tr>
      <tr>
        <td>Alamat</td><td>:</td><td><?php echo $p->alamat ." ".$p->kecamatan.", ". $p->kabupaten .", ".$p->provinsi ?></td>
      </tr>
      <tr>
        <td>Pendidikan</td><td>:</td><td><?php echo $p->pendidikan ?></td>
      </tr>
      <tr>
        <td>Pekerjaan</td><td>:</td><td><?php echo $p->pekerjaan ?></td>
      </tr>
      <tr>
        <td>Agama</td><td>:</td><td><?php echo $p->agama ?></td>
      </tr>
      <tr>
        <td>Status</td><td>:</td><td><?php echo $p->status ?></td>
      </tr>
      <tr>
        <td>No Telepon</td><td>:</td><td><?php echo $p->no_telp ?></td>
      </tr>
  </table>
  </div>
</div>
<a href="" onclick="window.print();return false">CETAK</a>
</center>
</body>
</html>
<?php } ?>